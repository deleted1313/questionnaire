# Опросник

## Стэк технологий

* Vue
* Vuex
* SCSS
* CSS Modules


## Установка

```shell
npm install
```

## Скрипты

* `npm run start` - Старт
* `npm run build` - Сборка проекта
* `npm run lint` - Запуск линтера

## API

* `https://thebestapi-intheworld.herokuapp.com/reasons` - get - get reasons
* `https://thebestapi-intheworld.herokuapp.com/label` - get - get label
* `https://thebestapi-intheworld.herokuapp.com/current_session` - get - get current session
* `https://thebestapi-intheworld.herokuapp.com/current_session` - put - update current session


