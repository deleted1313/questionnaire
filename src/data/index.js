export const langs = {
  send: "Отправить",
  neverAsk: "Больше не справшивать",
  close: "Закрыть",
  alertText: "Вы не прошли проверку",
  yourOpinionTitle: "Нам важно Ваше мнениe!",
  mainScreenTitle:
    "Мы хотим стать лучше! Пожалуйста, пройдите опрос и оцените качество сервиса",
  startQuestionnaire: "Пройти опрос",
  rateQuestionnaireScreenText:
    "Для оценки используйте 10-балльную шкалу, где 10 – точно готовы рекомендовать, 1 – точно не готовы рекомендовать.",
  reasonsQuestionnaireScreenTitle: "Почему Вы не поставили оценку 10?",
  reasonsQuestionnaireScreenText: "Выберите не более 3-х ключевых причин.",
  messageQuestionnaireScreenTitle: "Багодарим за оценку!",
  messageQuestionnaireScreenText:
    "Мы будем признательны, если Вы оставите отзыв или предложение по улучшению работы «Газпромнефть-Корпоративные продажи».",
  messageQuestionnaireScreenPlaceholder: "Ваш ответ…",
  lastScreenTitle: "Спасибо!"
};

export const baseSteps = [
  {
    content: {
      title: langs.yourOpinionTitle,
      label: "",
      text: langs.rateQuestionnaireScreenText,
      isStepRequired: false,
      buttons: [
        { text: langs.send, arg: 1, actionType: "send" },
        { text: langs.neverAsk, arg: null, actionType: "never_ask" }
      ]
    }
  },
  {
    content: {
      title: langs.reasonsQuestionnaireScreenTitle,
      label: "",
      text: langs.reasonsQuestionnaireScreenText,
      isStepRequired: true,
      buttons: [
        { text: langs.send, arg: 2, actionType: "send" },
        { text: langs.close, arg: null, actionType: "close" }
      ]
    }
  },
  {
    content: {
      title: langs.messageQuestionnaireScreenTitle,
      label: "",
      text: langs.messageQuestionnaireScreenText,
      isStepRequired: false,
      buttons: [
        { text: langs.send, arg: 3, actionType: "send" },
        { text: langs.close, arg: null, actionType: "close" }
      ]
    }
  },
  {
    content: {
      title: langs.lastScreenTitle,
      label: "",
      text: "",
      isStepRequired: false,
      buttons: [{ text: langs.close, arg: null, actionType: "finish" }]
    }
  }
];

export const apiUrl = "https://thebestapi-intheworld.herokuapp.com";
