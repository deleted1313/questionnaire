import Vuex from "vuex";
import Vue from "vue";
import { baseSteps, langs, apiUrl } from "../data";
Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    isQuestionnaireModalOpened: false,
    currentStep: 0,
    steps: [],
    reasons: [],
    reasonsCheckboxValues: [],
    message: null,
    isCurrentStepValid: false,
    isAlertShowed: false,
    isFetching: false,
    isLoaderVisible: true,
    ratesElements: Array(10).fill({ checked: true, hovered: false }),
    rateInputValue: 10,
    langs,
    baseSteps,
    apiUrl,
    isQuestionnaireAlertVisible: true
  },
  mutations: {
    startQuestionnaire(state) {
      state.isAlertShowed = false;
      state.rateInputValue = 10;
      state.ratesElements = Array(10).fill({ checked: true });
      state.reasonsCheckboxValues = [];
      state.message = null;
      return (state.isQuestionnaireModalOpened = true);
    },
    setSteps(state, steps) {
      state.steps = steps;
    },
    updateStep(state, step) {
      state.steps[0] = step;
    },
    setReasons(state, reasons) {
      state.reasons = reasons;
    },
    goToScreen(state, screen) {
      if (screen === null) {
        state.currentStep = 0;
        return (state.isQuestionnaireModalOpened = false);
      }
      state.isCurrentStepValid = false;
      state.isAlertShowed = false;
      return (state.currentStep = screen);
    },
    handleChangeTextarea(state, e) {
      return (state.message = e.target.value);
    },
    handleChangeRateInput(state, index) {
      state.ratesElements = state.ratesElements.map((el, i) => {
        if (i <= index) {
          return { checked: true };
        }
        return { checked: false };
      });
      return (state.rateInputValue = state.ratesElements.filter(
        el => el.checked
      ).length);
    },

    showAlert(state) {
      return (state.isAlertShowed = !state.isAlertShowed);
    },

    hideQuestionnaireAlert(state) {
      return (state.isQuestionnaireAlertVisible = false);
    },

    handleChangeReasonsCheckbox(state, id) {
      if (state.reasonsCheckboxValues.includes(id)) {
        state.reasonsCheckboxValues.splice(
          state.reasonsCheckboxValues.indexOf(id),
          1
        );
      } else {
        state.reasonsCheckboxValues.push(id);
      }
      if (state.reasonsCheckboxValues.length > 0) {
        return (state.isCurrentStepValid = true);
      } else {
        return (state.isCurrentStepValid = false);
      }
    },
    startFetch(state, type) {
      return (state.isFetching = type);
    },
    endFetch(state) {
      return (state.isFetching = false);
    },
    endQuestionnaire(state) {
      state.currentStep = 0;
      state.rateInputValue = 10;
      state.reasonsCheckboxValues = [];
      state.message = null;
      state.isQuestionnaireAlertVisible = false;
      return (state.isQuestionnaireModalOpened = false);
    },
    hideLoader() {
      if (document.querySelector("#loader")) {
        return document.querySelector("#loader").remove();
      }
    }
  }
});
