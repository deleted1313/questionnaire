import { Modal } from "./components/Modal/Modal";
import ButtonsContainer from "./components/ButtonsContainer/ButtonsContainer";
import CurrentStep from "./components/CurrentStep/CurrentStep";
import Loader from "./components/Loader";
import { baseSteps, apiUrl, langs } from "./data";
import axios from "axios";
import { mapMutations } from "vuex";
import GpnLogo from "./assets/gpn-logo.svg";
import Opti24Logo from "./assets/opti-24-logo.svg";
import Close from "./assets/close.svg";
import s from "./App.scss";

export default {
  created: async function() {
    // Навешиваем коллбэки после создания экземпляра
    this.setSteps(
      baseSteps.map(step => ({
        ...step,
        content: {
          ...step.content,
          buttons: step.content.buttons.map(button => ({
            ...button,
            callback: () => this.goToScreen(button.arg)
          }))
        }
      }))
    );
    this.startFetch("label");
    const { data } = await axios.get(`${apiUrl}/reasons`);
    this.setReasons(data);
    const { data: label } = await axios.get(`${apiUrl}/label`);
    const newStep = this.$store.state.steps[0];
    newStep.content.label = label;
    this.updateStep(newStep);
    this.endFetch();
    this.hideLoader();
  },
  methods: {
    ...mapMutations([
      "startQuestionnaire",
      "goToScreen",
      "handleChangeTextarea",
      "handleChangeRateInput",
      "showAlert",
      "handleChangeReasonsCheckbox",
      "setSteps",
      "setReasons",
      "updateStep",
      "startFetch",
      "endFetch",
      "hideLoader",
      "hideQuestionnaireAlert"
    ])
  },
  render() {
    // Данные по текущему шагу опроса
    const {
      isQuestionnaireModalOpened,
      currentStep,
      steps,
      isFetching,
      isQuestionnaireAlertVisible
    } = this.$store.state;
    const currentStepData = steps[currentStep];
    return (
      <div class={s.container}>
        <div>
          <div class={s.navbar}>
            <Opti24Logo />
            <GpnLogo />
          </div>
          {isQuestionnaireAlertVisible && (
            <p class={s.questionnaireAlert}>
              <span class={s.mainScreenTitle}>{langs.mainScreenTitle}</span>
              <button class={s.startButton} onClick={this.startQuestionnaire}>
                {langs.startQuestionnaire}
              </button>
              <Close
                class={s.closeQuestionnaireAlert}
                onClick={this.hideQuestionnaireAlert}
              />
            </p>
          )}
        </div>

        <Modal
          currentStep={currentStep}
          isQuestionnaireModalOpened={isQuestionnaireModalOpened}
        >
          <div
            class={[
              s.modalContainer,
              currentStep === 3 && s.lastScreenContainer
            ]}
          >
            <p class={s.screenTitle}>{currentStepData.content.title}</p>
            {isFetching === "label" ? (
              <Loader />
            ) : (
              currentStepData.content.label && (
                <p class={s.screenLabel}> {currentStepData.content.label} </p>
              )
            )}
            {currentStepData.content.text && (
              <p class={s.screenText}>{currentStepData.content.text}</p>
            )}
            <CurrentStep />
            <ButtonsContainer />
          </div>
        </Modal>
        <div class={s.footer}>
          <Opti24Logo class={s.opti24Logo} />
          <GpnLogo class={s.gazpromLogo} />
        </div>
      </div>
    );
  }
};
