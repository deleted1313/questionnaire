import { mapMutations } from "vuex";
import { langs } from "../../data";
import { Dislike, Like } from "../SvgImages";
import s from "./CurrentStep.scss";

export default {
  methods: {
    ...mapMutations([
      "handleChangeTextarea",
      "handleChangeRateInput",
      "handleChangeReasonsCheckbox",
      "showAlert"
    ]),
    setElementUnhovered: function() {
      this.$store.state.ratesElements = this.$store.state.ratesElements.map(
        el => ({ ...el, hovered: false })
      );
    },
    setElementHovered: function(index) {
      this.$store.state.ratesElements = this.$store.state.ratesElements.map(
        (el, i) => {
          if (i <= index) {
            return { ...el, hovered: true };
          }
          return { ...el, hovered: false };
        }
      );
    }
  },

  render() {
    const {
      currentStep,
      reasons,
      reasonsCheckboxValues,
      message,
      ratesElements,
      isAlertShowed
    } = this.$store.state;
    return (
      <div class={s.currentStep}>
        {currentStep === 0 && (
          <div class={s.rateElementsContainer}>
            <Dislike />
            {ratesElements.map((el, index, arr) => (
              <div
                class={s.rateElementContainer}
                onMouseenter={() => this.setElementHovered(index)}
                onMouseleave={this.setElementUnhovered}
                onClick={() => this.handleChangeRateInput(index)}
              >
                <hr
                  class={[
                    index === 0 && s.hiddenHr,
                    el.hovered && index !== 0 && s.hoveredHr,
                    el.checked && s.activeHr
                  ]}
                />
                <span
                  class={[
                    s.rateElement,
                    el.checked && s.rateElementChecked,
                    el.hovered && s.rateElementHovered
                  ]}
                >
                  <span
                    class={[
                      s.count,
                      index === 9 && s.lastCount,
                      el.hovered && s.hoveredCount
                    ]}
                  >
                    {" "}
                    {index + 1}{" "}
                  </span>
                </span>
                <hr
                  class={[
                    el.hovered &&
                      arr[index + 1] &&
                      arr[index + 1].hovered &&
                      s.hoveredHr,
                    el.checked &&
                      arr[index + 1] &&
                      arr[index + 1].checked &&
                      s.activeHr,
                    index === 9 && s.hiddenHr
                  ]}
                />
              </div>
            ))}

            <Like />
          </div>
        )}
        {currentStep === 1 &&
          reasons.map(({ id, label }) => {
            return (
              <div>
                <label class={[s.checkboxContainer, s.label]}>
                  {label}
                  <input
                    type="checkbox"
                    class={s.checkbox}
                    id={id}
                    disabled={
                      reasonsCheckboxValues.length > 2 &&
                      !reasonsCheckboxValues.includes(id)
                    }
                    onChange={() => {
                      isAlertShowed && this.showAlert();
                      this.handleChangeReasonsCheckbox(id);
                    }}
                    checked={reasonsCheckboxValues.includes(id)}
                  />

                  <span class={s.checkmark} />
                </label>
              </div>
            );
          })}
        {currentStep === 2 && (
          <div>
            <textarea
              onChange={this.handleChangeTextarea}
              class={s.textarea}
              placeholder={langs.messageQuestionnaireScreenPlaceholder}
            >
              {message}
            </textarea>
          </div>
        )}
      </div>
    );
  }
};
