import s from "./Loader.scss";

export default {
  render() {
    const { size, hasOverlay } = this.$attrs;
    return hasOverlay ? (
      <div class={s.overlay}>
        <div class={[s.loader, s[size]]}>Loading...</div>
      </div>
    ) : (
      <div class={[s.loader, s[size]]}>Loading...</div>
    );
  }
};
