import { apiUrl, langs } from "../../data";
import axios from "axios";
import { mapMutations } from "vuex";
import s from "./ButtonsContainer.scss";
import Loader from "../Loader";
export default {
  methods: {
    ...mapMutations([
      "goToScreen",
      "showAlert",
      "endQuestionnaire",
      "startFetch",
      "endFetch",
      "hideQuestionnaireAlert"
    ]),

    sendData: async function(data) {
      return await axios.put(`${apiUrl}/set_current_session`, data);
    }
  },
  render() {
    const {
      currentStep,
      steps,
      rateInputValue,
      reasonsCheckboxValues,
      message,
      isCurrentStepValid,
      isAlertShowed,
      isFetching
    } = this.$store.state;
    const currentStepData = steps[currentStep];
    const { isStepRequired } = currentStepData.content;

    return (
      <div class={s.buttonsContainer}>
        {/* Проходимся по массиву кнопок текущего шага опросника, рендерим
            соответствующее кол-во кнопок */}
        {currentStepData.content.buttons.map(
          ({ actionType, callback, text }) => (
            <button
              class={[s.button, s[actionType]]}
              onClick={async () => {
                if (actionType === "never_ask" && !isFetching) {
                  this.endQuestionnaire();
                  return this.hideQuestionnaireAlert();
                }
                const requestBody = {
                  rate: rateInputValue,
                  reasons: reasonsCheckboxValues,
                  message: message
                };
                if (actionType === "close" && !isFetching) {
                  this.startFetch("close");
                  await this.sendData({
                    ...requestBody,
                    finish: false
                  });
                  this.endFetch();

                  return this.endQuestionnaire();
                }
                if (actionType === "finish" && !isFetching) {
                  this.startFetch("finish");
                  await this.sendData({
                    ...requestBody,
                    finish: true
                  });
                  this.endFetch();

                  return this.endQuestionnaire();
                }
                if (
                  actionType === "send" &&
                  !isCurrentStepValid &&
                  isStepRequired
                ) {
                  return this.showAlert();
                }
                if (
                  actionType === "send" &&
                  currentStep === 0 &&
                  rateInputValue === 10
                ) {
                  return this.goToScreen(2);
                }
                return callback();
              }}
            >
              {isFetching === actionType ? <Loader /> : text}
              {actionType === "send" && isAlertShowed && (
                <span class={[s.alert, isAlertShowed && s.showedAlert]}>
                  {langs.alertText}
                </span>
              )}
            </button>
          )
        )}
      </div>
    );
  }
};
