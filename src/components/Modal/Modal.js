import s from "./Modal.scss";

export const Modal = props => {
  const { currentStep, isQuestionnaireModalOpened } = props.props;
  return (
    <div class={isQuestionnaireModalOpened && s.overlay}>
      <div
        class={
          isQuestionnaireModalOpened
            ? [s.modal, s[`maxHeightForStep${currentStep}`]]
            : s.hidden
        }
      >
        {props.children}
      </div>
    </div>
  );
};
